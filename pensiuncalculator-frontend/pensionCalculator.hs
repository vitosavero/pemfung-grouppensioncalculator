module Main where

import Happstack.Server (Browsing(EnableBrowsing), nullConf, serveDirectory, simpleHTTP,dir)
import Control.Monad (msum)


main :: IO()
main = simpleHTTP nullConf $ msum [serveDirectory EnableBrowsing ["homepage.html"] "templates/homepage.html",dir "calculator" $ serveDirectory EnableBrowsing ["index.html"] "templates/index.html"]

