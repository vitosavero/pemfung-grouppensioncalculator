
> {-# LANGUAGE DeriveDataTypeable #-}

> module Main where
>
> import Control.Applicative ((<$>), (<*>))
> import Happstack.Server (port,Method(GET, POST),dir,method,ServerPart, badRequest, nullConf, ok, simpleHTTP)
> import Control.Monad (msum)
> import Happstack.Server.RqData (RqData, checkRq, getDataFn, look, lookRead)
> import Text.JSON.Generic


> data Project =
>    Project {
>        name :: String,
>        lastStatus :: String,
>        builds :: [Build]
>    }
>    deriving (Data, Typeable)

> data Build =
>    Build {
>        buildName :: String,
>        status :: String
>    }
>    deriving (Data, Typeable)

> data User =
>    User {
>        nama :: String,
>        age :: Double,
>        retirementage :: Double,
>        wealthAcquired :: Double,
>        weatlthNeeded :: Double,
>        defisit :: Double
>    }
>    deriving (Data, Typeable)


> data Chart =
>    Chart {
>        startingAge :: Double,
>        retirementAge :: Double,
>        simpananSebelumRetired :: [Double],
>        simpananSesudahRetired :: [Double],
>        deathAge :: Double
>    }
>    deriving (Data, Typeable)


> testProjectJSON = encodeJSON (Project "New Project" "Succeeded" [Build "2010-10-02 build 2" "Succeeded", Build "2010-10-01 build 1" "Failed"])





> dataNama :: RqData (String)
> dataNama = look "name"
> dataAge :: RqData (Double)
> dataAge = lookRead "age"
> dataAgePensiun :: RqData (Double)
> dataAgePensiun = lookRead "agePensiun"
> dataAgeDeath :: RqData (Double)
> dataAgeDeath = lookRead "ageDeath"


> dataUser :: RqData(String,Double,Double,Double,Double,Double,Double,Double,Double,Double,Double)
> dataUser = (,,,,,,,,,,) <$> dataNama <*> dataAge <*> dataAgePensiun <*> dataAgeDeath <*> dataSimpanan <*> dataInvestasiNow <*> dataInvestasiNowBunga <*> dataSimpananBulanan <*> dataSimpananBulananBunga <*> dataPengeluaranSebelumPensiun <*> dataPengeluaranSesudahPensiun

> dataTest :: RqData(String,Double,Double)
> dataTest = (,,) <$> dataNama <*> dataAge <*> dataAgePensiun

> dataSimpanan :: RqData (Double)
> dataSimpanan = lookRead "simpanan"
> dataInvestasiNow :: RqData (Double)
> dataInvestasiNow = lookRead "investasiNow"
> dataInvestasiNowBunga :: RqData (Double)
> dataInvestasiNowBunga = lookRead "investasiNowBunga"
> dataSimpananBulanan :: RqData (Double)
> dataSimpananBulanan = lookRead "simpananBulanan"
> dataSimpananBulananBunga :: RqData (Double)
> dataSimpananBulananBunga = lookRead "simpananBulananBunga"

> dataPengeluaranSebelumPensiun :: RqData (Double)
> dataPengeluaranSebelumPensiun = lookRead "pengeluaranSebelumPensiun"
> dataPengeluaranSesudahPensiun :: RqData (Double)
> dataPengeluaranSesudahPensiun = lookRead "pengeluaranSesudahPensiun"

> dataSaving :: RqData(Double, Double, Double, Double, Double)
> dataSaving = (,,,,) <$> dataAge <*> dataAgePensiun <*> dataSimpanan <*> dataSimpananBulanan <*> dataSimpananBulananBunga

> hitungUsia :: Double -> Double -> Double -> Double
> hitungUsia umurPensiun pemasukkan pengeluaran
>		| pemasukkan >= pengeluaran  = 1 + hitungUsia umurPensiun (pemasukkan - pengeluaran) pengeluaran
>		| otherwise                  = umurPensiun


> userPart :: ServerPart String
> userPart =
>     do r <- getDataFn dataUser
>        case r of
>          (Left e) ->
>              badRequest $ unlines e
>          (Right (name, umur,agePensiun,ageDeath,simpanan,din,dinb,dsb,dsbb,dpsp,dpspen)) ->
>              ok $ name ++ " Umur: " ++ show umur ++ " Target Pensiun: " ++ show agePensiun ++ " Target Kehidupan: " ++ show ageDeath ++ " " ++ show simpanan ++ " aa " ++ show dpspen



> outputPart :: ServerPart String
> outputPart =
>     do r <- getDataFn dataTest
>        case r of
>          (Left e) ->
>              badRequest $ unlines e
>          (Right (name, umur, agePensiun)) ->
>              ok $ encodeJSON(User name umur agePensiun 1000 500 500)

> chartPart :: ServerPart String
> chartPart =
>     do r <- getDataFn dataTest
>        case r of
>          (Left e) ->
>              badRequest $ unlines e
>          (Right (name, umur, agePensiun)) ->
>              ok $ encodeJSON(Chart umur agePensiun [100,100,100,100] [10,10,10] 10)




> financialPart :: ServerPart String
> financialPart =
>     do r <- getDataFn dataUser
>        case r of
>          (Left e) ->
>              badRequest $ unlines e
>          (Right (name, umur,agePensiun,ageDeath,simpanan,din,dinb,dsb,dsbb,dpsp,dpspen)) ->
>              ok $ "Sorry," ++ name ++ " Retirement Plan Coming Soon " ++ show (dsbb+dpspen) ++ " " ++ show (hitungUsia agePensiun simpanan din)
>					++ " " ++ show (truncate((countSavings simpanan dsb) !! (truncate(agePensiun - umur)*12)))

> ageDifference :: Double -> Double -> Double
> ageDifference dataAge dataAgePensiun = subtract dataAgePensiun dataAge

-- Bunga = SRH x i x t/365
-- SRH = Saldo Ra-rata perhari
-- i = suku bunga tabungan pertahun
-- t = jumlah hari dalam bulan berjalan


> calculateSavings :: Double -> Double -> Double
> calculateSavings dataSimpanan dataSimpananBulanan = dataSimpanan + ((dataSimpanan + dataSimpananBulanan) * ((/) (0.06 * 30) 365))

> countSavings :: Double -> Double -> [Double]
> countSavings dataSimpanan dataSimpananBulanan = (hasil : countSavings hasil dataSimpananBulanan)
> 												  where hasil = calculateSavings dataSimpanan dataSimpananBulanan

> simpananPensiun :: Double -> Double -> Double -> Double -> [Double]
> simpananPensiun da@dataAge dap@dataAgePensiun ds@dataSimpanan dsb@dataSimpananBulanan = (countSavings ds dsb)
>                           where selisihUmur = ageDifference dap da

-- Bunga = SRH x i x t/365
-- SRH = Saldo Ra-rata perhari
-- i = suku bunga tabungan pertahun
-- t = jumlah hari dalam bulan berjalan

> savingPart :: ServerPart String
> savingPart =
>       do r <- getDataFn dataSaving
>          case r of
>               (Left e) ->
>                   badRequest $ unlines e
>               (Right (umur, umurPensiun, simpanan, simpananBulanan, simpananBulananBunga)) ->
>                   ok $ "Umur Sekarang: " ++ show umur ++ " Umur Pensiun: " ++ show umurPensiun ++ " Simpanan Sekarang: "
>						 ++ show simpanan ++ " Simpanan Perbulan: " ++ show simpananBulanan ++ " Bunga Bulanan: " ++ show simpananBulananBunga

> conf = nullConf { port = 8080 }

> main :: IO ()
> main = simpleHTTP conf  $ msum
>        [ dir "user" $ do userPart,
>          dir "financials"  $ do financialPart,
>          dir "savings" $ do savingPart,
>          dir "output" $ do outputPart,
>          dir "chart" $ do chartPart,
>          do method GET
>             ok $ testProjectJSON
>        , do method POST
>             ok $ "You did a POST request.\n"
>        ]