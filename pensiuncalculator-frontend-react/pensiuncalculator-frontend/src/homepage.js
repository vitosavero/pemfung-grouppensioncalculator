import React, { Component, useState, useEffect } from 'react';
import { Route, Link, BrowserRouter as Router, Switch } from "react-router-dom";
import { render } from 'react-dom';
// import './style.css';

import Calc from './calculator-frontend'


function Homepage() {

  const [Name, setName] = useState(() =>
  window.localStorage.getItem('name') || ''
  );
  const [currentAge, setcurrentAge] = useState(() =>
  window.localStorage.getItem('currentAge') || ''
  );
  const [retiredAge, setretiredAge] = useState(() =>
  window.localStorage.getItem('retiredAge') || ''
  );
  const [lifeExpentancy, setlifeExpentancy] = useState(() =>
    ''
  );
  const [savings, setSavings] = useState(() =>
    ''
  );
  const [monthlySavings, setMonthlySavings] = useState(() =>
    ''
  );

  const handleNameChange = (e) => setName(e.target.value);
  const handleAgeChange = (e) => setcurrentAge(e.target.value);
  const handleretiredAgeChange = (e) => setretiredAge(e.target.value);
  const handlelifeExpentancyChange = (e) => setlifeExpentancy(e.target.value);
  const handlesavingsChange = (e) => setSavings(e.target.value);

  useEffect(() => {
    window.localStorage.setItem('name', Name), [Name];
    window.localStorage.setItem('currentAge', currentAge), [currentAge];
    window.localStorage.setItem('retiredAge', retiredAge), [retiredAge];
    window.localStorage.setItem('lifeExpanctancy', lifeExpentancy), [lifeExpentancy];
    window.localStorage.setItem('savings', savings), [savings];
  });

    return (
        <div class="uk-grid-collapse  uk-child-width-expand@s uk-text-center" uk-grid>
        <div class="uk-background-secondary uk-height-viewport uk-padding uk-light">
        <form action="http://localhost:8080/user/">
            <div class="uk-margin">
            <h1>Who are you? What are your plans?</h1>
            Name : <input class="uk-input uk-form-width-medium" type="text" name="name" placeholder="Enter your name"  value={Name} onChange={handleNameChange} required/><br/><br/>
            Current Age : <input class="uk-input uk-form-width-medium" type="text" name="age" placeholder="Enter your current age" value={currentAge} onChange={handleAgeChange}   required/><br/><br/>
            Retired Age : <input class="uk-input uk-form-width-medium" type="text" name="agePensiun" placeholder="Enter your retiring age" value={retiredAge} onChange={handleretiredAgeChange} required/><br/><br/>
            Life Expentancy : <input class="uk-input uk-form-width-medium" type="text" name="ageDeath" placeholder="Death age?" value={lifeExpentancy} onChange={handlelifeExpentancyChange} required/><br/><br/>
            </div>

            <div class="uk-margin">
            <h1>How is your money situation?</h1>
            Savings : <input class="uk-input uk-form-width-medium" type="text" name="simpanan" placeholder="Your Money In The Bank" required/><br/><br/>
            {/* Monthly Investments : <input class="uk-input uk-form-width-medium" type="text" name="investasiNow" required/>
            Return on Investments Target :  <input class="uk-input uk-form-width-medium" type="text" name="investasiNowBunga" required/><br/><br/> */}
            Monthly Savings : <input class="uk-input uk-form-width-medium" type="text" name="simpananBulanan" placeholder="Input Monthly Income" required/>
            Monthly Savings Interest : <input class="uk-input uk-form-width-medium" type="text" name="simpananBulananBunga" placeholder="Use Target Percentage" required/><br/><br/>
            </div>

            <div class="uk-margin">
            <h1>How do you plan to live before and after retirement?</h1>
             Living Cost Before Retirement Age : <input class="uk-input uk-form-width-medium" type="text" name="pengeluaranSebelumPensiun" placeholder="Your Current Spending"  required/><br/><br/>
            Expected Living Cost After  Retirment : <input class="uk-input uk-form-width-medium" type="text" name="pengeluaranSesudahPensiun" placeholder="Your Future Spending"  required/><br/><br/>

            </div>

            <h1>Click on analysis to see your what your future holds!</h1>
        </form>
        </div>
    </div>

    );
}


export default Homepage

render(<Homepage />, document.getElementById('root'));

