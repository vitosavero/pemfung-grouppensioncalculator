import React, { Component } from 'react';
import { render } from 'react-dom';
import { Route, Link, BrowserRouter as Router, Switch } from "react-router-dom";
import Calc from "./calculator-frontend";
import Homepage from "./homepage";
// import './style.css';

function App() {

    return (

      <div className="App">
      <Router>
       <div class="uk-grid-collapse  uk-child-width-expand@s uk-text-center" uk-grid>
           <div>
      <div class="uk-background-secondary uk-height-viewport uk-padding uk-light">
        <div class="uk-padding-large">
          <h1>💰</h1>
          <h1 class="uk-heading-hero">Pensiun Calculator</h1>

          <h2>Ready to plan out your life?</h2>
          <h3>
          With PenCal you will get an overview on how your savings and current spending will
          effect your future.</h3>
          <h4>
          Just input your data and we will take care of the rest!
          Click the button below to start planning out your future!
          </h4>
        </div>
            <div class="uk-padding-medium">
           <Link to="/homepage"> <button uk-tooltip="Here you can count the of money you need until you die!" class="uk-button uk-button-default uk-button-large">Input Data!</button>
          </Link>

          <Link to="/calculator"> <button uk-tooltip="Here you can count the of money you need until you die!" class="uk-button uk-button-default uk-button-large">Analysis!</button>
           </Link>

            </div>

            <Switch>
            <Route path="/calculator" component={Calc} />
            <Route path="/homepage" component={Homepage} />
            </Switch>

            </div>
          </div>
        </div>
        </Router>
      </div>
    );

}

render(<App />, document.getElementById('root'));



