import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Chart from "react-google-charts";


function Calc() {
  const [data, setData] = useState({ hits: [] });
  // const [data2, setData2] = useState({ hits: [] });
  const dataz = [
  ["Age", "Assets", { role: "style" }],
  ["20", 10, "color: green"],
  ["21", 12, "color: green"],
  ["22", 13, "color: green"],
  ["23", 14, "color: green"],
  ["24", 15, "color: green"],
  ["25", 16, "color: green"],
  ["26", 17, "color: green"],
  ["27", 18, "color: green"],
  ["28", 19, "color: green"],
  ["29", 20, "color: yellow"],
  ["30", 19, "color: green"],
  ["31", 18, "color: green"],
  ["31", 17, "color: green"],
  ["34", 16, "color: green"],
  ["35", 15, "color: green"],
  ["36", 14, "color: green"],
  ["37", 13, "color: green"],
  ["38", 12, "color: green"],
  ["39", 11, "color: green"],
  ["40", 10, "color: green"],
  ["41", 9, "color: green"],
  ["42", 8, "color: green"],
  ["43", 7, "color: green"],
  ["44", 6, "color: green"],
  ["45", 5, "color: green"],
  ["46", 3, "color: green"],
  ["47", 2, "color: green"],
  ["48", 1, "color: green"],
  ["49", 0, "color: red"],
  ["Pensiun Target", -1, "red"],
  ["51", -1, "red"],
  ["52", -2, "red"],
  ["53", -3, "red"],
  ["54", -4, "red"],
  ["55", -5, "red"],
  ["Death", -6, "red"],
  ];

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(
        'http://localhost:8080/output/',{
          params: {
          name:   window.localStorage.getItem('name'),
          age:   window.localStorage.getItem('currentAge'),
          agePensiun: window.localStorage.getItem('retiredAge')
         }
        }
      );
      console.log(result.data);
      setData(result.data);
    };
    fetchData();
    // const fetchData2 = async () => {
    //   const result2 = await axios.get(
    //     'http://localhost:8080/output/',{
    //       params: {
    //       name:   window.localStorage.getItem('name'),
    //       age:   window.localStorage.getItem('currentAge'),
    //       agePensiun: window.localStorage.getItem('retiredAge')
    //      }
    //     }
    //   );
    //   console.log(result2.data);
    //   setData2(result2.data);
    // };
    // fetchData2();
  }, []);
  return (
    <ul>
           <h1>Analysis</h1>
              <Chart chartType="ColumnChart" width="100%" height="400px" data={dataz} />
            <h2>
            Hey, {data.nama} It looks like your aged {data.age} and your planning to retire at {data.retirementage},
            {/* if else kalau defisit atau tidak */}
            Unfortunatly, if you keep on spending like this you wont make it until your death you need about {data.weatlthNeeded} Million Rupiahs
            if you want to make it till your target. But it is okay you are still young so stop spendin and start savin' from now!
          </h2>

    </ul>
  );
}
export default Calc;