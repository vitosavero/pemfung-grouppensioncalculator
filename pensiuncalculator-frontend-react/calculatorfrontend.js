import React, { useState, useEffect } from 'react';
import axios from 'axios';
function App() {
  const [data, setData] = useState({ hits: [] });
  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        'http://localhost:8080/output/?name=Ahmad+Yazid&age=30',
      );
      console.log(result.data);
      setData(result.data);
    };
    fetchData();
  }, []);
  return (
    <ul>
          <p>
            Hallo, {data.nama}  Jikalau Umur anda {data.age} dan anda berencana untuk pensiun pada umur {data.retirementage}
            {/* if else kalau defisit atau tidak */}
            Sayang nya, anda masih membutuhkan {data.weatlthNeeded} ribu lagi

          </p>

    </ul>
  );
}
export default App;