# Pemfung-GroupPensionCalculator

> Simple pensiun calculator to count your asset until retirement

1406565436 - Rezady Yan Hafiz Munaf
1506688872 - Vitosavero Avila Wibisono
1506722752 - Ahmad Yazid
1606918300 - Rahmat Hidayat
1706039692 - Ariq Munif Kusuma

## Slide
https://bit.ly/37lEsF8

### How To Build

## Requirements

- Haskell, version 8.0.2
- [Node.js v10.16.0](https://nodejs.org/en/download/releases/) and `npm` package


## Api and additional frameworks needed
- CABAL JSON
- CABAL HAPSTACK
- GOOGLE CHARTS API
- HAPSTACK
- REACT.js
- UI kit


## Installation

# Install Haskell

To install haskell follow instructions given at https://www.haskell.org/ghc/

# Install Hapstack

To install hapstack follow instructions given at http://happstack.com/page/view-page-slug/2/download

# Install Node.js and npm package manager

To install node.js and npm package manager at https://github.com/creationix/nvm


### Running The Calculator

# To run the frontend

```sh
$ cd pensiuncalculator-frontend-react/pensiuncalculator-frontend
$ npm install
$ npm start
```

# To run the backend

```sh
$ cd pensiuncalculator-backend
$ ghc --make -threaded PensiunCalculatorBackend.lhs -o pensiuncalculator
$ runhaskell PensiunCalculatorBackend.hs
```

If configured correctly the pensiun calculator could be accessed at http://localhost:3000/
